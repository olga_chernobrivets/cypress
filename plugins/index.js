const fs = require('fs-extra')
function getConfigurationByFile (file) {
  return fs.readJson('/home/olgach/course/cypress/config/'+file+'.json')
}

module.exports = (on, config) => {
  const file = config.env.configFile || 'dev'
  return getConfigurationByFile(file)
}

