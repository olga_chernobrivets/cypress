import {LoginPageAPI} from "../support/pageObjects/loginPageAPI";

describe('API tests', () => {
    it('Edit user skype and email', () => {
        LoginPageAPI.login_api()
            .then(() => {
                return LoginPageAPI.edit_user_personal_settings({skype:'shirochka2601-2',
                                                                 email_personal: 'cypress@test.com'})
            })
            .then(() =>{
                LoginPageAPI.logout_api()
            })
    })
})


